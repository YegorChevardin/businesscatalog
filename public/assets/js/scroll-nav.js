/* Scrolling navbar section start */
var prevScrollpos = window.pageYOffset;

window.onscroll = function() {
    var currentScrollpos = window.pageYOffset;

    if(currentScrollpos >= 15 * 16) {
        if(prevScrollpos > currentScrollpos) {
            document.getElementById('scroll-nav').style.top = "0";
        } else {
            document.getElementById('scroll-nav').style.top = "-5.1em";
        }
        prevScrollpos = currentScrollpos;
    } else {
        document.getElementById('scroll-nav').style.top = "-5.1em";
    }
}
/* Scrolling navbar section end */
