/* Todo list section start */
let link_input = document.getElementById('link-name');
let add_link_button = document.getElementById('add-link-button');
let links_wrapper = document.querySelector('#links-wrapper table tbody');

//Getting array of existing links
let links;
!localStorage.links ? links = [] : links = JSON.parse(localStorage.getItem('links'));

//Validation function for data that user send by link adding
function linkValidate(string) {
    let url;

    try {
        url = new URL(string);
    } catch (_) {
        return false;
    }

    //return url.protocol === "http:" || url.protocol === "https:";
    return true;
}

function Link(description) {
    this.description = description;
}

const createTemplate = (link, index) => {
    return `
            <tr>
                <td>${link.description.substring(0,60)}...</td>
                <td class="text-center d-flex justify-content-center align-items-center"><button onclick="deleteLink(${index})" type="button" class="btn btn-danger"><img src="assets/css/images/svg/trash.svg" alt="delete button" width="30" height="30"/></button></td>
            </tr>
        `
}

const fillHtmlList = () => {
    links_wrapper.innerHTML = "";

    if(links.length > 0) {
        links.forEach((item, index) => {
            links_wrapper.innerHTML += createTemplate(item, index);
        });
    }
}

window.onload = fillHtmlList();

const update_local = () => {
    localStorage.setItem('links', JSON.stringify(links));
}

add_link_button.addEventListener('click', () => {
    if(linkValidate(link_input.value) == false) {
        alert("Посилання не є правильним!");
        link_input.value = '';
    } else {
        links.push( new Link(link_input.value));
        update_local();
        fillHtmlList();
        link_input.value = '';
    }
});

const deleteLink = index => {
    links.splice(index, 1);
    update_local();
    fillHtmlList();
}
/* Todo list section end */
