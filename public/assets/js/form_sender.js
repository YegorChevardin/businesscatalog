$(document).ready(function() {
    $("#case-form").submit(function(e) {
        e.preventDefault();
        var th = $(this);
        var string = "";
        var input_links = document.getElementById('links');
        var project_links = JSON.parse(localStorage.getItem('links'));

        if(project_links.length > 0) {
            project_links.forEach((item, index) => {
                string += item.description + ",";
            });
        }

        input_links.value = string;

        var data = $(this).serializeArray();
        data.push({name: "project_links", value: project_links});

        $.ajax({
            type: "POST",
            url: "../public/project-send",
            data: $.param(data)
        }).done(function() {
            $("#redirect-to-tnx-page").submit();
        });
        return false;
    });
});
