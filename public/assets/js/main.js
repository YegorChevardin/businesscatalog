$(document).ready(function() {
    //Wow animation activation
    new WOW().init();

    /* Table accordion section start */
    var business_table = $('#business-table tbody .accordion-header');

    business_table.on('click', function() {
        var el = this,
            current_business_table_data = $("#business-table tbody .business-content[data-collapse-child='" + el.getAttribute("data-bs-target") + "']"),
            current_polygon = $("#business-table tbody .accordion-header[id='" + el.getAttribute("id") + "'] .polygon");
        if(el.classList.contains("collapsed") && current_business_table_data.hasClass("border-0") == false) {
            $("#business-table tbody .accordion-header .polygon").removeClass("polygon-rotate");
            el.removeClass("border-0");
            current_business_table_data.addClass("border-0");
        } else if(el.classList.contains("collapsed") && current_business_table_data.hasClass("border-0") == true) {
            $("#business-table tbody .accordion-header .polygon").removeClass("polygon-rotate");
            el.removeClass("border-0");
        } else {
            $("#business-table tbody .accordion-header .polygon").removeClass("polygon-rotate");
            current_polygon.addClass("polygon-rotate");
            $("#business-table tbody tr[id='" + el.getAttribute("id") + "'] td").addClass("border-0");
            current_business_table_data.removeClass("border-0");
        }
    });
    /* Table accordion section end */

    /* Magnific popup section start */
    $('header .header-bg button').magnificPopup({
        items: {
            src: '#case-form-container'
        }
    });
    /* Magnific popup section start */

    /* Swiper section start */
    var swiper = new Swiper(".mySwiper", {
        loop: true,
        spaceBetween: 10,
        slidesPerView: 4,
        freeMode: true,
        watchSlidesProgress: true
    });
    var swiper2 = new Swiper(".mySwiper2", {
        loop: true,
        spaceBetween: 10,
        navigation: {
            nextEl: ".swiper-button-next",
            prevEl: ".swiper-button-prev"
        },
        thumbs: {
            swiper: swiper
        },
        autoplay: {
            delay: 2500,
            disableOnInteraction: false
        }
    });
    /* Swiper section end */

    /* Back to top button section start */
    $("back-top-button").on('click', function(e) {
        e.preventDefault();
        $('html, body').animate({scrollTop:0}, '300');
    });
    /* Back to top button section end */
});
