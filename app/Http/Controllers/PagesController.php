<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Post;
use Illuminate\Support\Facades\View;

class PagesController extends Controller
{
    public function eco() {
        if(View::exists('eco')) {
            $posts = Post::where('category_id', 1)->where('status', 'published')->paginate(6);

            //Preparing slider images for post-single
            foreach ($posts as $post) {
                $post->slider_images = trim($post->image, '[]"');
                $post->slider_images = explode('","', $post->slider_images);
                $post->links = explode(',', $post->links);
            }

            return view('eco')->with([
                'posts' => $posts
            ]);
        }
        abort(404);
    }

    public function soc() {
        if(View::exists('soc')) {
            $posts = Post::where('category_id', 2)->where('status', 'published')->paginate(6);

            //Preparing slider images for post-single
            foreach ($posts as $post) {
                $post->slider_images = trim($post->image, '[]"');
                $post->slider_images = explode('","', $post->slider_images);
                $post->links = explode(',', $post->links);
            }

            return view('soc')->with([
                'posts' => $posts
            ]);
        }
        abort(404);
    }

    public function kop() {
        if(View::exists('kop')) {
            $posts = Post::where('category_id', 3)->where('status', 'published')->paginate(6);

            //Preparing slider images for post-single
            foreach ($posts as $post) {
                $post->slider_images = trim($post->slider_images, '[]"');
                $post->slider_images = explode('","', $post->slider_images);
                $post->links = explode(',', $post->links);
            }

            return view('kop')->with([
                'posts' => $posts
            ]);
        }
        abort(404);
    }

    public function char() {
        if(View::exists('char')) {
            $posts = Post::where('category_id', 4)->where('status', 'published')->paginate(6);

            //Preparing slider images for post-single
            foreach ($posts as $post) {
                $post->slider_images = trim($post->image, '[]"');
                $post->slider_images = explode('","', $post->slider_images);
                $post->links = explode(',', $post->links);
            }

            return view('char')->with([
                'posts' => $posts
            ]);
        }
        abort(404);
    }
}
