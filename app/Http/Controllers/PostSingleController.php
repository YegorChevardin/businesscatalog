<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Post;
use Illuminate\Support\Facades\View;

class PostSingleController extends Controller
{
    public function index($postId = 1) {
        if(View::exists('post_single')) {
            if(empty(Post::find($postId))) {
                abort(404);
            }

            $post = Post::find($postId);

            $post->slider_images = trim($post->slider_images, '[]"');
            $post->slider_images = explode('","', $post->slider_images);
            $post->links = explode(',', $post->links);

            return view('post_single', ['post' => $post]);
        }
        abort(404);
    }
}
