<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use App\Models\Post;

class ProjectSendController extends Controller
{
    public function index(Request $request) {
        if(View::exists('tnx') and !empty($request)) {
            //validation
            $data = $request->validate([
                'company-name' => 'required|max:30',
                'project-name' => 'required|max:50',
                'project-budget' => 'required|max:20',
                'project-description' => 'required|max:10000|min:30',
                'person-name' => 'required|max:30|',
                'person-email' => 'required|email',
                'person-phone' => 'required'
            ]);

            //inserting info into database
            $post = new Post();
            switch ($request->input('catalog')) {
                case "еко-свідомий бізнес":
                    $post->category_id = 1;
                    break;
                case "соц-відповідальний бізнес":
                    $post->category_id = 2;
                    break;
                case "корпоративний еко-логічний бізнес":
                    $post->category_id = 3;
                    break;
                case "еко-внесок мешканців міста":
                    $post->category_id = 4;
                    break;
                default:
                    $post->category_id = 1;
            }
            $post->company_name = $request->input('company-name');
            $post->project_name = $request->input('project-name');
            $post->project_description = $request->input('project-description');
            $post->short_project_description = $request->input('project-description');
            $post->status = 'draft';
            $post->starting_date = $request->input('starting-date');
            $post->ending_date = $request->input('ending-date');
            $post->project_budget = $request->input('project-budget');
            $post->person_name = $request->input('person-name');
            $post->person_email = $request->input('person-email');
            $post->person_phone = $request->input('person-phone');
            /* Links decoding section start */
            $post->links = $request->input('links');
            /* Links decoding section end */
            $post->save();

            return view('tnx');
        }
        abort(404);
    }
}
