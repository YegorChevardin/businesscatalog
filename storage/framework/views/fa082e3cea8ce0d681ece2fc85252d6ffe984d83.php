<?php $__env->startSection('content'); ?>
    <!-- Header section start -->
    <header>
        <div class="container">
            <div class="eco header-bg text-white p-5">
                <div class="row align-items-center h-100">
                    <div class="col-md-6">
                        <div class="header-content">
                            <h1>Вклад бізнесу в збереження довкілля</h1>
                            <p class="mt-4">
                                Ми зібрали найкращі практики бізнесу,
                                <br/>
                                які допомагають підприємцям
                                <br/>
                                турбуватися про довкілля
                            </p>
                            <button class="btn btn-lg btn-light eco-style mt-5">Додати кейс</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <!-- Header section end -->
    <!-- Main section start -->
    <main>
        <!-- Button section start -->
        <section id="catalog-buttons" class="mt-4">
            <div class="container">
                <div class="row justify-content-center align-items-center mb-4">
                    <div class="col-md-6 d-flex">
                        <button class="btn btn-light active eco-style w-100 pt-4 pb-4" onclick=window.location.href="<?php echo e(route('eco')); ?>">еко-свідомий бізнес</button>
                    </div>
                    <div class="col-md-6">
                        <button class="btn btn-light w-100 pt-4 pb-4" onclick=window.location.href="<?php echo e(route('soc')); ?>">соц-відповідальний бізнес</button>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <button class="btn btn-light w-100 pt-4 pb-4" onclick=window.location.href="<?php echo e(route('kop')); ?>">корпоративний еко-логічний бізнес</button>
                    </div>
                    <div class="col-md-6">
                        <button class="btn btn-light w-100 pt-4 pb-4" onclick=window.location.href="<?php echo e(route('char')); ?>">еко-внесок мешканців міста</button>
                    </div>
                </div>
            </div>
        </section>
        <!-- Button section end -->
        <!-- Table section start -->
        <section id="business-table-section" class="mt-4">
            <div class="container">
                <?php if(isset($posts[0])): ?>
                    <div class="table-responsive bg-white pb-5">
                        <table id="business-table" class="table m-0 table-condensed">
                            <thead class="text-white eco-style">
                            <tr>
                                <th class="border-0 ps-4"></th>
                                <th class="border-0 p-4 pe-3 ps-0">
                                    <div class="filter-button">
                                        <span>Назва</span>
                                    </div>
                                </th>
                                <th class="border-0 p-4 pe-3 ps-0 text-center">
                                    <div class="filter-button">
                                        <span>Дата</span>
                                        <img class="filter-polygon" src="<?php echo e(asset('assets/css/images/svg/Polygon-white.svg')); ?>" width="16px" height="9px" alt="polygon"/>
                                    </div>
                                </th>
                                <th class="border-0 p-4 pe-0 ps-0 text-end">
                                    <div class="filter-button">
                                        <span>Бюджет</span>
                                        <img class="filter-polygon" src="<?php echo e(asset('assets/css/images/svg/Polygon-white.svg')); ?>" width="16px" height="9px" alt="polygon"/>
                                    </div>
                                </th>
                                <th class="border-0 pe-4"></th>
                            </tr>
                            </thead>
                            <tbody class="accordion" id="accordionExample">
                            <?php $__currentLoopData = $posts; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $post): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <!-- Table element section start -->
                                <tr class="accordion-header" id="heading<?php echo e($post->id); ?>" class="accordion-button" data-bs-toggle="collapse" data-bs-target="#collapse<?php echo e($post->id); ?>" aria-expanded="true" aria-controls="collapse<?php echo e($post->id); ?>">
                                    <th class="border-0 ps-4"></th>
                                    <td class="p-4 pe-3 ps-0" data-id-parent="#heading<?php echo e($post->id); ?>">
                                        <img class="polygon" src="<?php echo e(asset('assets/css/images/svg/Polygon-black.svg')); ?>" width="16px" height="9px" alt="polygon"/>
                                        <span><?php echo e($post->company_name); ?></span>
                                    </td>
                                    <td class="p-4 pe-3 ps-0 text-center"><span class="start-date"><?php echo e(date('Y', strtotime($post->starting_date))); ?></span> — <span class="end-date"><?php echo e(date('Y', strtotime($post->ending_date))); ?></span></td>
                                    <td class="p-4 pe-0 ps-0 text-end"><span class="budget"><?php echo e($post->project_budget); ?></span>₴</td>
                                    <th class="border-0 pe-4"></th>
                                </tr>
                                <tr>
                                    <th class="border-0 p-0 ps-4"></th>
                                    <td colspan="3" class="p-0 business-content border-0" data-collapse-child="#collapse<?php echo e($post->id); ?>">
                                        <div id="collapse<?php echo e($post->id); ?>" class="accordion-collapse collapse" aria-labelledby="heading<?php echo e($post->id); ?>" data-bs-parent="#accordionExample">
                                            <div class="container">
                                                <div class="row justify-content-between m-0">
                                                    <?php if(!empty($post->image)): ?>
                                                        <div class="col-md-5">
                                                            <div class="img-wrapper w-100 h-auto m-0 p-0 mb-3">
                                                                <img src="<?php echo e(asset('storage'.DIRECTORY_SEPARATOR.$post->image)); ?>" alt="<?php echo e($post->company_name); ?>" class="img-fluid"/>
                                                            </div>
                                                        </div>
                                                    <?php endif; ?>
                                                    <div class="col-md-7">
                                                        <h3><?php echo e($post->project_name); ?></h3>
                                                        <p class="post-text text-left"><?php echo e($post->short_project_description); ?></p>
                                                        <?php if(isset($post->links[0])): ?>
                                                            <div class="m-0 mt-3 mb-3">
                                                                <p>Посилання на джерела проекту:</p>
                                                                <?php $__currentLoopData = $post->links; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $link): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                                    <a href="<?php echo e($link); ?>" target="_blank"><?php echo e($link); ?></a>
                                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                            </div>
                                                        <?php endif; ?>
                                                        <div class="button-wrapper mb-3">
                                                            <a class="btn btn-lg btn-dark" href="post-single/<?php echo e($post->id); ?>">Узнать подробнее</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                    <th class="border-0 p-0 pe-4"></th>
                                </tr>
                                <!-- Table element section end -->
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="row justify-content-center align-items-center mt-5">
                        <div class="col-md-12 text-center">
                            <div class="d-flex justify-content-center align-items-center">
                                <?php echo e($posts->links()); ?>

                            </div>
                        </div>
                    </div>
                <?php else: ?>
                    <div class="row justify-content-center align-items-center mt-5">
                        <div class="col-md-6">
                            <h2 class="text-black mt-5 text-center">На данний момент каталог порожній</h2>
                        </div>
                    </div>
                <?php endif; ?>
            </div>
        </section>
        <!-- Table section end -->
    </main>
    <!-- Main section end -->
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.template', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /opt/lampp/htdocs/businesscatalog/resources/views/eco.blade.php ENDPATH**/ ?>