<section id="case-form-section">
    <div id="case-form-container" class="position-relative container mx-auto mfp-hide">
        <div class="wrapper bg-light p-5 mt-5 mb-5">
            <div class="popup-header mb-5">
                <h2 class="text-center">Додати кейс до онлайн-каталогу</h2>
            </div>
            <div class="popup-body">
                <form id="redirect-to-tnx-page" action="<?php echo e(route('tnx')); ?>" method="post" class="d-none"><?php echo csrf_field(); ?></form>
                <form id="case-form" action="<?php echo e(route('project-send')); ?>" method="post">
                    <?php echo csrf_field(); ?>
                    <div class="mb-3">
                        <label for="catalog-select" class="form-label">Каталог</label>
                        <select id="catalog-select" class="form-select" name="catalog" required>
                            <option>еко-свідомий бізнес</option>
                            <option>соц-відповідальний бізнес</option>
                            <option>корпоративний еко-логічний бізнес</option>
                            <option>еко-внесок мешканців міста</option>
                        </select>
                    </div>
                    <div class="mb-3">
                        <label for="company-name" class="form-label">Назва компанії</label>
                        <input type="text" class="form-control p-2" id="company-name" name="company-name" required/>
                    </div>
                    <div class="mb-3">
                        <label for="project-name" class="form-label">Назва проекту / ініціативи</label>
                        <input type="text" class="form-control p-2" id="project-name" name="project-name" required/>
                    </div>
                    <div class="mb-3">
                        <label for="project-budget" class="form-label">Бюджет проекту (грн.)</label>
                        <input type="number" class="form-control" id="project-budget" name="project-budget" required/>
                    </div>
                    <!-- Starting and ending date section start -->
                    <div class="mb-3">
                        <label for="starting-date" class="form-label">Дата початку роботи над проектом</label>
                        <input type="datetime-local" class="form-control" id="starting-date" name="starting-date" required/>
                    </div>
                    <div class="mb-3">
                        <label for="ending-date" class="form-label">Дата кінця роботи над проектом</label>
                        <input type="datetime-local" class="form-control" id="ending-date" name="ending-date" required/>
                    </div>
                    <!-- Starting and ending date section end -->
                    <div class="mb-3">
                        <label for="project-description" class="form-label">Опис проекту</label>
                        <textarea class="form-control" id="project-description" name="project-description" required></textarea>
                    </div>
                    <!-- ToDo list section start -->
                    <div class="mb-3">
                        <label class="form-label">Посилання на джерело / публікації проекту (через кому, без пробілу)</label>
                        <input type="url" class="form-control p-2" id="link-name" placeholder="Введіть сюди посилання"/>
                        <input class="d-none" id="links" name="links"/>
                        <div class="d-flex align-items-center justify-content-center w-100">
                            <button id="add-link-button" type="button" class="btn btn-primary mt-2 mb-2 border-radius-30 shadow-lg w-100">Додати посилання до списку</button>
                        </div>
                        <div id="links-wrapper" class="table-responsive">
                            <table class="table table-striped table-hover table-bordered table-sm table-light">
                                <tbody></tbody>
                            </table>
                        </div>
                    </div>
                    <!-- ToDo list section end -->
                    <div class="mb-3">
                        <label for="person-name" class="form-label">Ваше ім’я та прізвище</label>
                        <input type="text" class="form-control" id="person-name" name="person-name" required/>
                    </div>
                    <div class="mb-3">
                        <label for="person-email" class="form-label">Єлектронна пошта</label>
                        <input type="email" class="form-control" id="person-email" name="person-email" required/>
                    </div>
                    <div class="mb-3">
                        <label for="person-phone" class="form-label">Номер телефона</label>
                        <input placeholder="099 9999 999" type="tel" class="form-control" id="person-phone" name="person-phone" required/>
                    </div>
                    <div class="d-flex align-items-center justify-content-center">
                        <button type="submit" class="btn btn-light text-white
                            <?php if(\Route::current()->getName() == 'eco'): ?>
                                eco-style
                            <?php elseif(\Route::current()->getName() == 'soc'): ?>
                                soc-style
                            <?php elseif(\Route::current()->getName() == 'kop'): ?>
                                kop-style
                            <?php elseif(\Route::current()->getName() == 'char'): ?>
                                char-style
                            <?php endif; ?>
                         mt-5 w-100 p-3">Надіслати</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
<!--
Code for post pages (links)
<div class="post-links mt-4 mb-3">
    <a href="" class="text-black d-block text-black">Facebook</a>
    <a href="" class="text-black d-block">Сайт компании</a>
</div>
-->
<?php /**PATH /opt/lampp/htdocs/businesscatalog/resources/views/layouts/form.blade.php ENDPATH**/ ?>