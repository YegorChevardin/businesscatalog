<footer class="pt-5">
    <div class="container">
        <div class="bg-white p-5 pt-4">
            <!-- Partners section start -->
            <section id="partners" class="mb-5">
                <div class="row align-items-center justify-content-between">
                    <div class="col-md-3 text-center mb-1">
                        <img src="<?php echo e(asset('assets/css/images/partners/Arnika.png')); ?>" alt="Арніка" class="w-50"/>
                    </div>
                    <div class="col-md-3 text-center mb-1">
                        <img src="<?php echo e(asset('assets/css/images/partners/Transition.png')); ?>" alt="Transition" class="w-100"/>
                    </div>
                    <div class="col-md-3 text-center mb-1">
                        <img src="<?php echo e(asset('assets/css/images/partners/CleanAir.png')); ?>" alt="Clean Air" class="w-50"/>
                    </div>
                    <div class="col-md-3 text-center mb-1">
                        <img src="<?php echo e(asset('assets/css/images/partners/Nikopol.png')); ?>" alt="Міжнародна Екологічна безпека" class="w-50"/>
                    </div>
                </div>
                <p class="text-center mt-4">
                    Онлайн-каталог розроблений проектом «Nikopol CLEAN Air Monitor — проект моніторинга повітря» та ГО «Міжнародна екологічна безпека» в рамках програми малих грантів Кампанії за чисте повітря в українських містах ("Чисте повітря для України»), за підтримки чеської неурядової організації ARNIKA та фінансується Міністерством закордонних справ Чеської Республіки в рамках Програми трансформаційної співпраці.
                </p>
            </section>
            <!-- Partners section end -->
            <p id="rights" class="text-center mb-0">© All rights reserved</p>
        </div>
    </div>
</footer>
<?php /**PATH /opt/lampp/htdocs/businesscatalog/resources/views/layouts/footer.blade.php ENDPATH**/ ?>