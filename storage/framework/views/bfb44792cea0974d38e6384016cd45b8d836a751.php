<link rel="shortcut icon" href="<?php echo e(asset('assets/css/images/favicon.png')); ?>" type="image/x-icon"/>
<link rel="stylesheet" href="<?php echo e(asset('assets/libs/bootstrap/css/bootstrap.css')); ?>"/>
<link rel="stylesheet" href="<?php echo e(asset('assets/css/bootstrap-skins.css')); ?>"/>
<!-- Magnific popup section start -->
<link rel="stylesheet" href="<?php echo e(asset('assets/libs/magnific-popup/magnific-popup.css')); ?>"/>
<!-- Magnific popup section end -->
<!-- Carousel section start -->
<link rel="stylesheet" href="https://unpkg.com/swiper@7/swiper-bundle.min.css"/>
<!-- Carousel section end -->
<!-- Wow animation section start -->
<link rel="stylesheet" type="text/css" href="<?php echo e(asset('assets/libs/wow-animation/animate.css')); ?>"/>
<!-- Wow animation section end -->
<link rel="stylesheet" href="<?php echo e(asset('assets/css/styles.css')); ?>"/>
<?php /**PATH /opt/lampp/htdocs/businesscatalog/resources/views/layouts/css.blade.php ENDPATH**/ ?>