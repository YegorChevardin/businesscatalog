@extends('layouts.template')
@section('content')
    <!-- Navbar section start -->
    <nav id="scroll-nav" class="navbar navbar-expand-lg navbar-light bg-light shadow-lg sticky-top">
        <div class="container">
            <a class="navbar-brand" href="#">
                <img class="me-3" src="{{ asset('assets/css/images/partners/Nikopol.png') }}" alt="logo" width="125"/>
                <strong>Бизнес каталог</strong>
            </a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav ms-auto mb-2 mb-lg-0">
                    <li class="nav-item">
                        <a class="nav-link active text-primary" href="{{ route('eco') }}">На главную</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    <!-- Navbar section end -->
    <!-- Main section start -->
    <main>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="info mb-5">
                        <p class="text-black date wow bounceInLeft" data-wow-delay="0.1s">{{ $post->created_at }}</p>
                        <h2 class="text-dark wow bounceInLeft" data-wow-delay="0.2s">
                            {{ $post->company_name }}
                        </h2>
                        <p class="text-dark place mt-4 wow bounceInLeft" data-wow-delay="0.3s">Період роботи над проектом: <span class="start-date">{{date('Y', strtotime($post->starting_date))}}</span> — <span class="end-date">{{date('Y', strtotime($post->ending_date))}}</span></p>
                        <p class="text-dark place mt-2 wow bounceInLeft" data-wow-delay="0.3s">Бюджет, який було закладено на проект: <span class="budget">{{ $post->project_budget }}</span>₴</p>
                    </div>
                </div>
            </div>
            <div class="row">
                @if(!empty($post->slider_images[0]))
                    <div class="col-md-6">
                        <div class="sliders-wrapper w-100 h-auto wow bounceInUp" data-wow-delay="0.1s">
                            <div style="--swiper-navigation-color: #fff;" class="swiper mySwiper2">
                                <div class="swiper-wrapper">
                                    @foreach($post->slider_images as $slider_image)
                                        <div class="swiper-slide">
                                            <img src="{{ asset('storage'.DIRECTORY_SEPARATOR.$slider_image) }}" alt=""/>
                                        </div>
                                    @endforeach
                                </div>
                                <div class="swiper-button-next"></div>
                                <div class="swiper-button-prev"></div>
                            </div>
                            <div thumbsSlider="" class="swiper mySwiper">
                                <div class="swiper-wrapper">
                                    @foreach($post->slider_images as $slider_image)
                                        <div class="swiper-slide">
                                            <img src="{{ asset('storage'.DIRECTORY_SEPARATOR.$slider_image) }}" alt=""/>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                @endif
                <div class="col-md-6">
                    <h3 class="wow bounceInLeft">{{ $post->project_name }}</h3>
                    <div class="content wow bounceInUp" data-wow-delay="0.2s">
                        @if(isset($post->links[0]))
                            <div class="m-0 mt-3 mb-3">
                                <p>Посилання на джерела проекту:</p>
                                @foreach($post->links as $link)
                                    <a href="{{ $link }}" target="_blank">{{ $link }}</a>
                                @endforeach
                            </div>
                        @endif
                        <p>
                            <?php
                                echo $post->project_description;
                            ?>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </main>
    <!-- Main section end -->
@endsection
