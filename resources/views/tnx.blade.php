@extends('layouts.template')
@section('content')
    <!-- Main section start -->
    <main class="bg-white">
        <div class="tnx-container">
            <div class="container h-100">
                <div class="row h-100 align-items-center justify-content-center text-center">
                    <div class="col-md-6">
                        <div class="wow bounceIn">
                            <h1 class="text-dark mb-5">Дякуємо за співпрацю!</h1>
                            <p>
                                Ваш кейс успішно був відправлений на модерацію, ми доповімо вам по email про публікацію.<br/>
                                Оператор може набрати вас, якщо виникнуть деякі питання.
                            </p>
                            <div class="d-flex flex-wrap align-items-center justify-content-around">
                                <a class="text-success" href="{{ route('soc') }}">Переглянути інші кейси</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
    <!-- Main section end -->
@endsection
