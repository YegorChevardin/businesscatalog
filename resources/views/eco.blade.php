@extends('layouts.template')
@section('content')
    <!-- Header section start -->
    <header>
        <div class="container">
            <div class="eco header-bg text-white p-5">
                <div class="row align-items-center h-100">
                    <div class="col-md-6">
                        <div class="header-content">
                            <h1>Вклад бізнесу в збереження довкілля</h1>
                            <p class="mt-4">
                                Ми зібрали найкращі практики бізнесу,
                                <br/>
                                які допомагають підприємцям
                                <br/>
                                турбуватися про довкілля
                            </p>
                            <button class="btn btn-lg btn-light eco-style mt-5">Додати кейс</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <!-- Header section end -->
    <!-- Main section start -->
    <main>
        <!-- Button section start -->
        <section id="catalog-buttons" class="mt-4">
            <div class="container">
                <div class="row justify-content-center align-items-center mb-4">
                    <div class="col-md-6 d-flex">
                        <button class="btn btn-light active eco-style w-100 pt-4 pb-4" onclick=window.location.href="{{ route('eco') }}">еко-свідомий бізнес</button>
                    </div>
                    <div class="col-md-6">
                        <button class="btn btn-light w-100 pt-4 pb-4" onclick=window.location.href="{{ route('soc') }}">соц-відповідальний бізнес</button>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <button class="btn btn-light w-100 pt-4 pb-4" onclick=window.location.href="{{ route('kop') }}">корпоративний еко-логічний бізнес</button>
                    </div>
                    <div class="col-md-6">
                        <button class="btn btn-light w-100 pt-4 pb-4" onclick=window.location.href="{{ route('char') }}">еко-внесок мешканців міста</button>
                    </div>
                </div>
            </div>
        </section>
        <!-- Button section end -->
        <!-- Table section start -->
        <section id="business-table-section" class="mt-4">
            <div class="container">
                @if(isset($posts[0]))
                    <div class="table-responsive bg-white pb-5">
                        <table id="business-table" class="table m-0 table-condensed">
                            <thead class="text-white eco-style">
                            <tr>
                                <th class="border-0 ps-4"></th>
                                <th class="border-0 p-4 pe-3 ps-0">
                                    <div class="filter-button">
                                        <span>Назва</span>
                                    </div>
                                </th>
                                <th class="border-0 p-4 pe-3 ps-0 text-center">
                                    <div class="filter-button">
                                        <span>Дата</span>
                                        <img class="filter-polygon" src="{{ asset('assets/css/images/svg/Polygon-white.svg') }}" width="16px" height="9px" alt="polygon"/>
                                    </div>
                                </th>
                                <th class="border-0 p-4 pe-0 ps-0 text-end">
                                    <div class="filter-button">
                                        <span>Бюджет</span>
                                        <img class="filter-polygon" src="{{ asset('assets/css/images/svg/Polygon-white.svg') }}" width="16px" height="9px" alt="polygon"/>
                                    </div>
                                </th>
                                <th class="border-0 pe-4"></th>
                            </tr>
                            </thead>
                            <tbody class="accordion" id="accordionExample">
                            @foreach($posts as $post)
                                <!-- Table element section start -->
                                <tr class="accordion-header" id="heading{{ $post->id }}" class="accordion-button" data-bs-toggle="collapse" data-bs-target="#collapse{{ $post->id }}" aria-expanded="true" aria-controls="collapse{{ $post->id }}">
                                    <th class="border-0 ps-4"></th>
                                    <td class="p-4 pe-3 ps-0" data-id-parent="#heading{{ $post->id }}">
                                        <img class="polygon" src="{{ asset('assets/css/images/svg/Polygon-black.svg') }}" width="16px" height="9px" alt="polygon"/>
                                        <span>{{ $post->company_name }}</span>
                                    </td>
                                    <td class="p-4 pe-3 ps-0 text-center"><span class="start-date">{{date('Y', strtotime($post->starting_date))}}</span> — <span class="end-date">{{date('Y', strtotime($post->ending_date))}}</span></td>
                                    <td class="p-4 pe-0 ps-0 text-end"><span class="budget">{{ $post->project_budget }}</span>₴</td>
                                    <th class="border-0 pe-4"></th>
                                </tr>
                                <tr>
                                    <th class="border-0 p-0 ps-4"></th>
                                    <td colspan="3" class="p-0 business-content border-0" data-collapse-child="#collapse{{ $post->id }}">
                                        <div id="collapse{{ $post->id }}" class="accordion-collapse collapse" aria-labelledby="heading{{ $post->id }}" data-bs-parent="#accordionExample">
                                            <div class="container">
                                                <div class="row justify-content-between m-0">
                                                    @if(!empty($post->image))
                                                        <div class="col-md-5">
                                                            <div class="img-wrapper w-100 h-auto m-0 p-0 mb-3">
                                                                <img src="{{ asset('storage'.DIRECTORY_SEPARATOR.$post->image) }}" alt="{{ $post->company_name }}" class="img-fluid"/>
                                                            </div>
                                                        </div>
                                                    @endif
                                                    <div class="col-md-7">
                                                        <h3>{{ $post->project_name }}</h3>
                                                        <p class="post-text text-left">{{ $post->short_project_description }}</p>
                                                        @if(isset($post->links[0]))
                                                            <div class="m-0 mt-3 mb-3">
                                                                <p>Посилання на джерела проекту:</p>
                                                                @foreach($post->links as $link)
                                                                    <a href="{{ $link }}" target="_blank">{{ $link }}</a>
                                                                @endforeach
                                                            </div>
                                                        @endif
                                                        <div class="button-wrapper mb-3">
                                                            <a class="btn btn-lg btn-dark" href="post-single/{{ $post->id }}">Узнать подробнее</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                    <th class="border-0 p-0 pe-4"></th>
                                </tr>
                                <!-- Table element section end -->
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="row justify-content-center align-items-center mt-5">
                        <div class="col-md-12 text-center">
                            <div class="d-flex justify-content-center align-items-center">
                                {{ $posts->links() }}
                            </div>
                        </div>
                    </div>
                @else
                    <div class="row justify-content-center align-items-center mt-5">
                        <div class="col-md-6">
                            <h2 class="text-black mt-5 text-center">На данний момент каталог порожній</h2>
                        </div>
                    </div>
                @endif
            </div>
        </section>
        <!-- Table section end -->
    </main>
    <!-- Main section end -->
@endsection
