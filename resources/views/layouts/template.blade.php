<!doctype html>
<html lang="ua">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport"
              content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Каталог бізнесу</title>
        <!-- Styles section start -->
        @include('layouts.css')
        <!-- Styles section end -->
    </head>
    <body>
        <!-- Back to top button section start -->
        <a id="back-top-button" class="btn btn-light p-2 shadow-sm">
            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-caret-up-fill" viewBox="0 0 16 16">
                <path d="m7.247 4.86-4.796 5.481c-.566.647-.106 1.659.753 1.659h9.592a1 1 0 0 0 .753-1.659l-4.796-5.48a1 1 0 0 0-1.506 0z"/>
            </svg>
        </a>
        <!-- Back to top button section end -->
        <!-- errors section start -->
        @if($errors->any())
            <div class="container">
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            </div>
        @endif
        <!-- errors section end -->
        <!-- Case form section start -->
        @include('layouts.form')
        <!-- Case form section end -->
        <!-- Content section start -->
        @yield('content')
        <!-- Content section end -->
        <!-- Footer section start -->
        @include('layouts.footer')
        <!-- Scripts section start -->
        @include('layouts.scripts')
        <!-- Scripts section end -->
        <!-- Footer section end -->
    </body>
</html>
