<link rel="shortcut icon" href="{{ asset('assets/css/images/favicon.png') }}" type="image/x-icon"/>
<link rel="stylesheet" href="{{ asset('assets/libs/bootstrap/css/bootstrap.css') }}"/>
<link rel="stylesheet" href="{{ asset('assets/css/bootstrap-skins.css') }}"/>
<!-- Magnific popup section start -->
<link rel="stylesheet" href="{{ asset('assets/libs/magnific-popup/magnific-popup.css') }}"/>
<!-- Magnific popup section end -->
<!-- Carousel section start -->
<link rel="stylesheet" href="https://unpkg.com/swiper@7/swiper-bundle.min.css"/>
<!-- Carousel section end -->
<!-- Wow animation section start -->
<link rel="stylesheet" type="text/css" href="{{ asset('assets/libs/wow-animation/animate.css') }}"/>
<!-- Wow animation section end -->
<link rel="stylesheet" href="{{ asset('assets/css/styles.css') }}"/>
