<script type="text/javascript" src="{{ asset('assets/libs/JQuery.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/libs/bootstrap/js/bootstrap.js') }}" defer></script>
<!-- Carousel scripts section start -->
<script src="https://unpkg.com/swiper@7/swiper-bundle.min.js"></script>
<!-- Carousel scripts section end -->
<!-- Magnific popup section start -->
<script type="text/javascript" src="{{ asset('assets/libs/magnific-popup/jquery.magnific-popup.js') }}" defer></script>
<!-- Magnific popup section end -->
<!-- Wow animation section start -->
<script type="text/javascript" src="{{ asset('assets/libs/wow-animation/wow.min.js') }}" defer></script>
<!-- Wow animation section end -->
<script type="text/javascript" src="{{ asset('assets/js/main.js') }}" defer></script>
<!-- Links adder for posts pages with form section starts -->
@if(\Route::current()->getName() != 'post-single' and \Route::current()->getName() != 'project-send')
    <script type="text/javascript" src="{{ asset('assets/js/links_adder.js') }}" defer></script>
    <script type="text/javascript" src="{{ asset('assets/js/form_sender.js') }}" defer></script>
@endif
<!-- links adder section ends -->
<!-- Scroll-nav section for post-single start -->
@if(\Route::current()->getName() == 'post-single')
    <script type="text/javascript" src="{{ asset('assets/js/scroll-nav.js') }}" defer></script>
@endif
<!-- Scroll-nav section end -->
