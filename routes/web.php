<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PagesController;
use App\Http\Controllers\PostSingleController;
use App\Http\Controllers\ProjectSendController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [PagesController::class, 'eco'])->name('eco');
Route::get('/eco', function() {
    return redirect(route('eco'));
});

Route::get('/soc', [PagesController::class, 'soc'])->name('soc');

Route::get('/kop', [PagesController::class, 'kop'])->name('kop');

Route::get('/char', [PagesController::class, 'char'])->name('char');

Route::get('/post-single/{id?}', [PostSingleController::class, 'index'])->name('post-single');

Route::post('/project-send', [ProjectSendController::class, 'index'])->name('project-send');
Route::get('/project-send', function() {
    abort(404);
});

Route::post('/tnx', function() {
    return view('tnx');
})->name('tnx');
Route::get('/tnx', function() {
    return abort(404);
});

Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});
